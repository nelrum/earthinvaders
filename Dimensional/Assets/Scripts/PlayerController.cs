﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    [Range(0, 20f)]
    public float speed;

    private Animator anim;

    private int maxHealth;
    public Transform laserPoint;
    public GameObject laserPrefab;

    private Rigidbody2D rb;
    private float horizontal;
    private float vertical;
    private Vector2 direction;
    
    [SerializeField] private UnityEvent onPlayerDeath = new UnityEvent();

    public AudioSource impactSFX;
    public AudioSource deathSFX;
    
    private void Start()
    {
        maxHealth = GameStats.PlayerHealth;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    private void SetMovement()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
    }

    private void FaceMouse()
    {
        Vector2 mouseScreenPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        direction = (mouseScreenPosition - (Vector2) transform.position).normalized;
        transform.up = -direction;
    }

    private void Shoot()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            var bullet = Instantiate(laserPrefab, laserPoint.position, laserPoint.rotation);
            bullet.GetComponent<Rigidbody2D>().velocity = -laserPoint.up * 10f;
            // Debug.Log("Shoot in mouse pos " + direction);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Health") && GameStats.PlayerHealth != maxHealth)
        {
            Destroy(other.gameObject);
            GameStats.PlayerHealth++;
        }
        if (other.CompareTag("Bullet") && GameStats.PlayerHealth > 0)
        {
            Destroy(other.gameObject);
            anim.SetTrigger("hurt");
            impactSFX.Play();
            GameStats.PlayerHealth--;
        }
    }

    private void Update()
    {
        if (GameStats.PlayerHealth <= 0 && !GameStats.IsDead)
        {
            Death();
            GameStats.IsDead = true;
        }
        SetMovement();
        FaceMouse();
        Shoot();
    }

    private void Death()
    {
        if (GameStats.IsDead) return;
        deathSFX.Play();
        anim.SetTrigger("death");
        Destroy(gameObject, 1.15f);
        StartCoroutine(InvokeOnPlayerDeath());
        GameStats.IsDead = true;
    }

    private IEnumerator InvokeOnPlayerDeath()
    {
        yield return new WaitForSeconds(1.05f);
        onPlayerDeath?.Invoke();
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(horizontal * speed, vertical * speed);
    }
}
