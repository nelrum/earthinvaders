﻿using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private Transform player;
    public float speed;
    public float distanceLimiter;

    public Transform bulletPoint;
    public GameObject bulletPrefab;

    public Animator animator;

    public AudioSource impactSFX;
    
    private Rigidbody2D rb;
    private float elapseTime;

    private bool isDead = false;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    private void Shoot()
    {
        elapseTime += Time.deltaTime;
        if (elapseTime >= 1.5f)
        {
            var bullet = Instantiate(bulletPrefab, bulletPoint.position, bulletPoint.rotation);
            bullet.GetComponent<Rigidbody2D>().velocity = bulletPoint.right * 10f;
            elapseTime = 0;
        }

    }

    private void FacePlayer()
    {
        var direction = player.position - transform.position;
        var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        rb.rotation = angle;
    }

    private void MoveToPlayer()
    {
        if (Vector2.Distance(transform.position, player.position) > distanceLimiter)
            transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
    }

    private void Update()
    {
        if (!isDead && !GameStats.IsDead)
        {
            Shoot();
            FacePlayer();
            MoveToPlayer();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Laser") && !isDead)
        {
            GameStats.Score += 100 * GameStats.Multiplier;
            GameStats.CurrentEnemiesDefeated++;
            GameStats.EnemiesDefeated++;
            impactSFX.Play();
            animator.SetTrigger("death");
            Destroy(gameObject, 1.25f);
            Destroy(other.gameObject, 1.25f);
            isDead = true;
        }
    }
}
