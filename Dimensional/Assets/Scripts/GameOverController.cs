﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverController : MonoBehaviour
{
    private Text text;

    private void Start()
    {
        text = GetComponent<Text>();
    }

    private void Update()
    {
        text.text = "GAME OVER\nEnemies Defeated: " + GameStats.EnemiesDefeated + "\nMax Score: " + GameStats.Score;
    }
}