﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(DrawGizmosBox))]
public class Spawner : MonoBehaviour
{
    public GameObject prefab;
    public float interval = 5f;
    [SerializeField] private  float elapsedTime = 0.0f;
    public float limit;

    private void Update()
    {
        if (!GameStats.IsDead)
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime > interval)
            {
                elapsedTime = 0;
                SpawnEnemy();
            }
        }
    }

    private void SpawnEnemy()
    {
        var spawnPosition = transform.position + new Vector3(Random.Range(-limit, limit), Random.Range(-limit, limit));
        Instantiate(prefab, spawnPosition, Quaternion.identity);
    }
}
