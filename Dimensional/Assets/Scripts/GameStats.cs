﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameStats
{
    public static int PlayerHealth { get; set; }
    
    public static bool IsDead { get; set; }

    public static int Score { get; set; }

    public static int Multiplier { get; set; }

    public static float MultiplierTimer { get; set; }

    public static int EnemiesDefeated { get; set; }
    
    public static int CurrentEnemiesDefeated { get; set; }
}
