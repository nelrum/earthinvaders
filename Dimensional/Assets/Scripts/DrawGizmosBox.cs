﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// [ExecuteInEditMode]
public class DrawGizmosBox : MonoBehaviour
{
    public float limit;

    public enum GizmosColors
    {
        Red,
        Blue,
        Green
    }

    public GizmosColors gizmosColors;

    private void OnDrawGizmos()
    {
        switch (gizmosColors)
        {
            case GizmosColors.Blue:
                Gizmos.color = Color.blue;
                break;
            case GizmosColors.Green:
                Gizmos.color = Color.green;
                break;
            case GizmosColors.Red:
                Gizmos.color = Color.red;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        Gizmos.DrawLine(transform.position + new Vector3(-limit, -limit), transform.position + new Vector3(-limit, limit));
        Gizmos.DrawLine(transform.position + new Vector3(limit, -limit), transform.position + new Vector3(limit, limit));
        Gizmos.DrawLine(transform.position + new Vector3(-limit, -limit), transform.position + new Vector3(limit, -limit));
        Gizmos.DrawLine(transform.position + new Vector3(limit, limit), transform.position + new Vector3(-limit, limit));
    }
}
