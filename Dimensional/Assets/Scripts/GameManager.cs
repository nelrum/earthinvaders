﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private void Start()
    {
        GameStats.PlayerHealth = 3;
        GameStats.IsDead = false;
        GameStats.Multiplier = 1;
        GameStats.Score = 0;
        GameStats.EnemiesDefeated = 0;
        GameStats.MultiplierTimer = 0.0f;
        GameStats.CurrentEnemiesDefeated = 0;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    private void Update()
    {
        if (GameStats.Multiplier > 1)
        {
            GameStats.MultiplierTimer += Time.deltaTime;
            if (GameStats.MultiplierTimer >= 5f)
            {
                GameStats.Multiplier = 1;
                GameStats.MultiplierTimer = 0.0f;
            }
        }
        if (GameStats.CurrentEnemiesDefeated > 2)
        {
            GameStats.Multiplier++;
            GameStats.CurrentEnemiesDefeated = 0;
            GameStats.MultiplierTimer = 0.0f;
        }
    }
}
