﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Image health;
    public Text score;
    public Text scoreMultiplier;
    public Text multiplierTimer;

    private void Update()
    {
        Health();
        Score();
        ScoreMultiplier();
        MultiplierTimer();
    }

    private void MultiplierTimer()
    {
        if (GameStats.Multiplier > 1)
        {
            var countDown = 5f - GameStats.MultiplierTimer;
            countDown = Mathf.Round(countDown * 10f) / 10f;
            multiplierTimer.text = "Multiplier Timer: " + countDown + "s";
        }
    }

    private void ScoreMultiplier()
    {
        scoreMultiplier.text = "Multiplier: " + GameStats.Multiplier + "x";
    }

    private void Score()
    {
        score.text = GameStats.Score.ToString();
    }
    
    private void Health()
    {
        health.fillAmount = GameStats.PlayerHealth / 3f;
    }
}
